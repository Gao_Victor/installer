###    **部署完成实例化测试验证** 


**1.安装完成后的检查** 

使用ansible部署Edgegallery成功后会有部署提示，如果没有部署则表示部署过程中某个模块部署失败因而部署脚本退出

kubectl get pod --all-namespaces  			// 检查pod的运行状态，正常情况下pod的状态时running，

如果pod 的状态不是running 则需要进一步定位，例如：

kubectkl  delete pod podname –n namespaces   // 重启pod

kubectl describe pod podname –n namespaces   // 查看pod

最后一步则需要手动实例化测试

 **2.手动实例化测试** 

EdgeGallery web页面登陆需要用Chrome浏览器

 **2.1 设置沙箱环境** 

EdgeGallery新增了超级管理员权限，目前申请的用户都为租户，需要通过管理员权限设置修改权限

账号：admin  密码：Admin@321 建议登录后修改密码

登录网址: https://PORTAL_IP:30091   PORTAL_IP 是安装时配置的PORTAL_IP 的IP（未配置PORTAL IP默认为master IP）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/091816_0ecd2fa5_8040887.png "屏幕截图.png")

管理员账号可以对租户进行管理：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/091955_aea2f129_8040887.png "屏幕截图.png")

在developer界面使用管理员权限账号添加沙箱环境：

登录网址: https://PORTAL_IP:30092  

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/092205_f11417cf_8040887.png "屏幕截图.png")

添加K8S或者openstack沙箱环境（端口号为mecm注册mepm系统的端口）：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/092817_22fa9092_8040887.png "屏幕截图.png")

 K8S上传的config配置文件为：root/.kube/下的config文件
 
 openstack沙箱环境为openstack的相关配置文件信息，将下面文件编辑成config文件上传：

 export OS_USERNAME=admin    

 export OS_PASSWORD=******

 export OS_PROJECT_NAME=admin

 export OS_AUTH_URL=http://192.168.*.*/identity

 export OS_IDENTITY_API_VERSION=3

 export OS_PROJECT_DOMAIN_NAME=default

 export OS_USER_DOMAIN_NAME=default

 **2.2 登录MECM界面** 

a)登录网址: https://PORTAL_IP:30093   PORTAL_IP 是安装时在env.sh配置的PORTAL_IP 的IP

b)在登录网址后看到的是MECM的概览页面此时登录用的guest用户 ，只有浏览页面的权限，不能执行有效的操作，需要点击右上角的“登录”跳转到登录页面

输入账号、密码、确认密码、电话、勾选我已同意并阅读、点击同意协议：

输入账号密码，拖动滚动条完成验证并点击登录：


 **2.3 使用管理员账号进行外部系统注册** 

MEPM注册

登录MECM后，点击系统→MEPM注册系统

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/093950_01854989_8040887.png "屏幕截图.png")

点击‘新增注册’注册MEPM ,  IP地址为边缘节点IP,多节点安装只需输入master node IP即可，端口是31252

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/094100_86437875_8040887.png "屏幕截图.png")


应用市场注册：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/094254_26a3bf51_8040887.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/094714_fd8036db_8040887.png "屏幕截图.png")

边缘节点注册

点击边缘节点→新增注册

输入IP地址为边缘节点IP,多节点安装只需输入master节点IP即可,选择安装环境的架和硬件能力

（没有可以不选）硬件品牌和型号,地址经纬度，选择MEPM ,填写边缘IP，输入边缘仓库端口

点击确认

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/094944_41aab39b_8040887.png "屏幕截图.png")

配置文件上传：

配置文件为要的边缘节点/root/.kube/ 下config文件，下载config文件，点击上传文件，看到提示‘你已成功上传配置文件’，则上传成功

（openstack则上传自己配置的config文件）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/095501_e6047bc0_8040887.png "屏幕截图.png")


 **2.4 在Developer上传镜像** 

 上传容器镜像：
 
 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100341_16b294ae_8040887.png "屏幕截图.png")

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100155_5bb6ff48_8040887.png "屏幕截图.png")
 
 上传虚机镜像：

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100445_a69c865f_8040887.png "屏幕截图.png")

 ![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100521_800463f4_8040887.png "屏幕截图.png")

 **2.5 在Developer页面部署测试** 

网址:https://PORTAL_IP:30092   PORTAL_IP 是安装时在env.sh配置的PORTAL_IP 的IP

添加新项目：

登录后点击“工作空间”→“创建项目”→“应用集成” 

!![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/095732_5e439793_8040887.png "屏幕截图.png")

按照上述选择完成项目创建。


上传yaml文件（支持模板下载）：

!![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100749_0c73b6af_8040887.png "屏幕截图.png")

开始部署:

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/100913_9ec39468_8040887.png "屏幕截图.png")

部署完成：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103317_90809020_8040887.png "屏幕截图.png")


点击应用认证进行安全，遵从性以及沙箱测试：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103447_0d198095_8040887.png "屏幕截图.png")

测试报告：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103518_bb6f8a69_8040887.png "屏幕截图.png")

等待前面测试完成后，点击发布即可发布到APP应用商店

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103652_353cff9f_8040887.png "屏幕截图.png")

 **3 虚机部署**

 下面部署虚机部署与容器部署不一样的地方（如需基于虚拟机开发可参考虚机开发docs仓库下/Projects/Developer/vm_app_guide.md文档）

创建项目是选择虚拟机：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103838_9f45dbe3_8040887.png "屏幕截图.png")

创建虚拟机名称：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/103947_2b2a5aaa_8040887.png "屏幕截图.png")

规格选择：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/104132_ec2735dc_8040887.png "屏幕截图.png")

网络配置：

!![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/104258_c8d5fc8c_8040887.png "屏幕截图.png")

参数配置生成应用包：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/104438_e8610f59_8040887.png "屏幕截图.png")

部署调测：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/113937_1d97b2a4_8040887.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/122547_1b012f76_8040887.png "屏幕截图.png")

应用代码上传与远程登录虚拟机：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/122732_d3055799_8040887.png "屏幕截图.png")


部署配置完成后可以生成镜像：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0926/122847_80866ead_8040887.png "屏幕截图.png")


虚机部署需要在openstack上提前配置好网络：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0708/104328_650256cf_8040887.png "屏幕截图.png")

虚机机规格建议和EG中规格一致：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0708/104547_33ecf2a9_8040887.png "屏幕截图.png")

 **3.appstore的在线体验功能** 

3.1 在appstore页面打开应用在线体验功能。

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/093212_644f5659_8040887.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/093311_78400e52_8040887.png "屏幕截图.png")

打开在线体验功能：

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/093900_878b9f37_8040887.png "屏幕截图.png")

3.2 设置在线体验沙箱环境

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/094110_3ed9c0b6_8040887.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/094140_2d1b0cc3_8040887.png "屏幕截图.png")


3.3 在我的应用中选择要在线体验的应用

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/094536_2103b2b9_8040887.png "屏幕截图.png")

3.4 在线体验部署完成后根据IP+端口进行访问体验，体验完成后释放资源

![输入图片说明](https://images.gitee.com/uploads/images/2021/0928/095117_7a3e0643_8040887.png "屏幕截图.png")




